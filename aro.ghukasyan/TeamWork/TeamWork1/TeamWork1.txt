The Object Oriented Programming (OOP) use programming objects which represent represent features and subjects of the real life 
in our surroundings. Both real life objects and programming objects have their attributes, behaviors and features. 
Taking this into consideration,  we can draw parallels between a wrist watch as a subject and the  OOP. Thus, 
we can say that all wrist watches are objects. Watch attributes are are such details of the clock as the style (analog or digital), 
color, the wrist band etc. It typically defines how the watch looks like and how its behavior is going to be. 
The class of the watch is its behavior and appearance that allow us to call it “watch”. So the class o f the watch is it type 
of the object. The behavior of the watch can include the time setting mechanism, the alarm system etc. As programming objects, 
watches can also inherit such features like the display of the time, the ability to be set for alarm etc. The abstraction 
can include features that other types of objects traditionally have such as computers and phones.We can surely say that the watch 
has its encapsulation as it hides its internal part from the user. The user can control some features but cannot directly interfere 
with its internal function. These internal parts can be considered as data members and functions that are hidden from the users 
like the information hiding in OOP.       
