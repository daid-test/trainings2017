#include <iostream>


int
main()
{
    int numberOne, numberTwo;

    std::cout << "Enter First Number: ";
    std::cin  >> numberOne;
    std::cout << "Enter Second Number: ";
    std::cin  >> numberTwo;

    if (0 == numberTwo) {
        std::cout << "Error 1: Division by zero\nThe number can't be divided by 0." << std::endl;
        return 1;
    }
    std::cout << numberOne << " + " << numberTwo << " = " << (numberOne + numberTwo) << std::endl;
    std::cout << numberOne << " - " << numberTwo << " = " << (numberOne - numberTwo) << std::endl;
    std::cout << numberOne << " * " << numberTwo << " = " << (numberOne * numberTwo) << std::endl;
    std::cout << numberOne << " / " << numberTwo << " = " << (numberOne / numberTwo) << std::endl;

    return 0;
}

