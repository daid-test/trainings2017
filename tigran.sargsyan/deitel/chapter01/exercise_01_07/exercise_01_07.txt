It is so much attention focused on the OOP (and on the C++ especially) because we live in a world in which "objects" are the main concepts. It is easier to operate with them because we see them every day, we know their properties and functions. It makes them more operative and understandable.

