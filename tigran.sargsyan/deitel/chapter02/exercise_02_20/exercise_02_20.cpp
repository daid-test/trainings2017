#include <iostream>

int
main()
{
    int R;

    std::cout << "Enter radius: ";
    std::cin >> R;

    std::cout << "Diameter: " << 2 * R << std::endl;
    std::cout << "Circumference: " << 2 * 3.14159 * R << std::endl;
    std::cout << "Area: " << 3.14159 * R * R << std::endl;

    return 0;
}

