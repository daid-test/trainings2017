#include <iostream>

int
main()
{
    std::cout << "Integer equivalent of A is " << static_cast<int>('A') << std::endl;
    std::cout << "Integer equivalent 0f B is " << static_cast<int>('B') << std::endl;
    std::cout << "Integer equivalent of C is " << static_cast<int>('C') << std::endl;
    std::cout << "Integer equivalent of a is " << static_cast<int>('a') << std::endl;
    std::cout << "Integer equivalent of b is " << static_cast<int>('b') << std::endl;
    std::cout << "Integer equivalent of c is " << static_cast<int>('c') << std::endl;
    std::cout << "Integer equivalent of 0 is " << static_cast<int>('0') << std::endl;
    std::cout << "Integer equivalent of 1 is " << static_cast<int>('1') << std::endl;
    std::cout << "Integer equivalent of 2 is " << static_cast<int>('2') << std::endl;
    std::cout << "Integer equivalent of dollar is " << static_cast<int>('$') << std::endl;
    std::cout << "Integer equivalent of multiple is " << static_cast<int>('*') << std::endl;
    std::cout << "Integer equivalent of plus is " << static_cast<int>('+') << std::endl;
    std::cout << "Integer equivalent of slash is " << static_cast<int>('/') << std::endl;
    std::cout << "Integer equivalent of space is " << static_cast<int>(' ') << std::endl;
    return 0;
}
